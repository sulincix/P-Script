#!/bin/bash
p-config(){
	if [ -f /etc/p-modules/p-config/$* ]
	then
		/etc/p-modules/p-config/$*
		exit 0
	else
		echo "Config not found"
		exit 1
	fi	

}
p-reload(){
	if [ -f /etc/p-modules/p-reload/$* ]
	then
		/etc/p-modules/p-reload/$*
		exit 0
	else
		echo "Module not found"
		exit 1
	fi
}
p-restart(){
	if [ -f /etc/p-modules/p-restart/$* ]
	then
		/etc/p-modules/p-restart/$*
		exit 0
	else
		echo "Module not found"
		exit 1
	fi
}
p-start(){
	if [ -f /etc/p-modules/p-start/$* ]
	then
		/etc/p-modules/p-start/$*
		exit 0
	else
		echo "Module not found"
		exit 1
	fi
}
p-status(){
	cat /var/log/p-script/$1.log
	exit 0
}
p-stop(){
	if [ -f /etc/p-modules/p-stop/$* ]
	then
		/etc/p-modules/p-stop/$*
		exit 0
	else
		echo "Module not found"
		exit 1
	fi
}
p-enable(){
  echo "$1" >> /etc/p-modules/p-start/null
  echo "$1" >> /etc/p-modules/p-reload/null
  echo "$1" >> /etc/p-modules/p-restart/null
  echo "$1" >> /etc/p-modules/p-config/null
  echo "$1" >> /etc/p-modules/p-stop/null
}
p-disable(){
  sed -i "/$1/d" /etc/p-modules/p-start/null
  sed -i "/$1/d" /etc/p-modules/p-reload/null
  sed -i "/$1/d" /etc/p-modules/p-restart/null
  sed -i "/$1/d" /etc/p-modules/p-config/null
  sed -i "/$1/d" /etc/p-modules/p-stop/null
}
