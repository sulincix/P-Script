DESTDIR=/
install:
	mkdir -p $(DESTDIR)/etc/init.d || true
	mkdir -p $(DESTDIR)/usr/bin || true
	mkdir -p $(DESTDIR)/usr/lib || true
	mkdir -p $(DESTDIR)/etc/p-modules/p-start/ || true
	mkdir -p $(DESTDIR)/etc/p-modules/p-reload/ || true
	mkdir -p $(DESTDIR)/etc/p-modules/p-restart/ || true
	mkdir -p $(DESTDIR)/etc/p-modules/p-config/ || true
	mkdir -p $(DESTDIR)/etc/p-modules/p-stop/ || true
	install p-script.service $(DESTDIR)/etc/init.d/p-script
	install p-script $(DESTDIR)/usr/bin/p-script
	install p-functions.sh $(DESTDIR)/usr/lib/p-functions.sh
